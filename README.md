# README #

This is a python Flask Application starter with gunicorn wsgi http server for Docker container.

### Setup and installation ###
1. install Docker in your system
2. open up a terminal/Docker QuickStart Terminal to start docker
   > docker --version
   > docker-compose --version
3. git clone or download this repo to your project root
4. cd to the project root
5. Build docker image and start docker container and server:
   > docker-compose up --build

6. stop and re-start docker container and server:
   > docker-compose stop
   > docker-compose up

6. Find the IP and visit browser:
   > docker-machine ip

7. start to create Flask Application

8. more docker commands:
   >  docker --help

   > docker-compose stop ContainerID#
   > docker-compose stop

   > docker-compose up
   > docker images

   > docker-compose ps
   > docker ps

   > docker-compose rm -f
   > docker rmi -f $(docker images -qf dangling=true)